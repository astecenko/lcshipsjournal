unit u6;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  DbCtrls;

type

  { TFrmAddGood }

  TFrmAddGood = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    DBLookupComboBox1: TDBLookupComboBox;
    procedure Button3Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

implementation
uses variants;

{$R *.lfm}

{ TFrmAddGood }

procedure TFrmAddGood.Button3Click(Sender: TObject);
begin
  ShowMessage(VarToStr(DBLookupComboBox1.KeyValue));
end;

end.

