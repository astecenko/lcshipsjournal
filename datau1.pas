unit datau1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, sqldb, FileUtil, ZConnection, ZDataset;

const
    scCity = 'City';
    scCityVisit = 'CityVisit';
    scPurchaseLot = 'PurchaseLot';
    scPurchaseLotView = 'PurchaseLotView';
    scGoodsCategory = 'GoodsCategory';
    scGoods = 'Goods';
    scSaleLot = 'SaleLot';
    scSaleLotView = 'SaleLotView';
    scValuta = 'Valuta';

type

  { TDM1 }

  TDM1 = class(TDataModule)
    DSPurchaseLotView: TDataSource;
    DSSaleLotView: TDataSource;
    DSValuta: TDataSource;
    DSGoods: TDataSource;
    DSSaleLot: TDataSource;
    DSCityVisit: TDataSource;
    DSGoodsCategory: TDataSource;
    DSPurchaseLot: TDataSource;
    DSCity: TDataSource;
    Conn: TZConnection;
    TableGoodsCategory: TZTable;
    TablePurchaseLot: TZTable;
    TableCity: TZTable;
    TableCityVisit: TZTable;
    TableSaleLot: TZTable;
    TableGoods: TZTable;
    TableValuta: TZTable;
    TableSaleLotView: TZTable;
    TablePurchaseLotView: TZTable;
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  DM1: TDM1;

implementation

{$R *.lfm}

end.

