unit u1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Menus,
  ActnList, StdActns;

const
  scFileName = 'data.sqlite3';

type

  { TFrmMain }

  TFrmMain = class(TForm)
    ActionNewVisit: TAction;
    ActionOpenDB: TAction;
    ActionGoodsOpen: TAction;
    ActionAbout: TAction;
    ActionList1: TActionList;
    FileExit1: TFileExit;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    procedure ActionAboutExecute(Sender: TObject);
    procedure ActionGoodsOpenExecute(Sender: TObject);
    procedure ActionNewVisitExecute(Sender: TObject);
    procedure ActionOpenDBExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  FrmMain: TFrmMain;

implementation
    uses datau1,u3,u4,LazFileUtils;

{$R *.lfm}

{ TFrmMain }

procedure TFrmMain.MenuItem2Click(Sender: TObject);
begin

end;

procedure TFrmMain.MenuItem6Click(Sender: TObject);
begin

end;

procedure TFrmMain.ActionAboutExecute(Sender: TObject);
begin
  ShowMessage('Судовой журнал Luck Catchers'#10#13'Тестовая версия');
end;

procedure TFrmMain.ActionGoodsOpenExecute(Sender: TObject);
var
  Form1 :TFrmGoods;
begin
     Form1:=TFrmGoods.Create(Self);
     Form1.ShowModal;
end;

procedure TFrmMain.ActionNewVisitExecute(Sender: TObject);
var
  Frm1:TFrmNewVisit;
begin
     Frm1:= TFrmNewVisit.Create(Self);
     Frm1.ShowModal;
     FreeAndNil(Frm1);
end;

procedure TFrmMain.ActionOpenDBExecute(Sender: TObject);
var
  s:String;
begin
  s := ExtractFilePath(Application.ExeName)+scFileName;
  if FileExists(s) then
     begin
          DM1.Conn.Database:=s;
          try
             DM1.Conn.Connect;
             DM1.TableCity.Open;
             DM1.TableCityVisit.Open;
             DM1.TablePurchaseLot.Open;
             DM1.TableSaleLot.Open;
             DM1.TableGoodsCategory.Open;
             DM1.TableGoods.Open;
             DM1.TableValuta.Open;
          except
            ShowMessage('Нет доступа к ' + s + ' работа невозможна');
            FileExit1.Execute;
          end;
     end
    else
     begin
       ShowMessage('Нет доступа к ' + s + ' работа невозможна');
       FileExit1.Execute;
     end;
end;

procedure TFrmMain.FormCreate(Sender: TObject);
begin

end;

procedure TFrmMain.MenuItem13Click(Sender: TObject);
begin

end;

end.

