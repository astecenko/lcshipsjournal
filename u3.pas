unit u3;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, DBDateTimePicker, Forms, Controls, Graphics,
  Dialogs, DbCtrls, DBGrids, ExtCtrls, ComCtrls, StdCtrls;

type

  { TFrmNewVisit }

  TFrmNewVisit = class(TForm)
    Button1: TButton;
    DBDateTimePicker1: TDBDateTimePicker;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DBGrid3: TDBGrid;
    DBGrid4: TDBGrid;
    DBLookupComboBox1: TDBLookupComboBox;
    DBNavigator1: TDBNavigator;
    DBNavigator2: TDBNavigator;
    PageControl1: TPageControl;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Splitter1: TSplitter;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    procedure Button1Click(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

implementation
uses u6;

{$R *.lfm}

{ TFrmNewVisit }

procedure TFrmNewVisit.Button1Click(Sender: TObject);
var
  Frm1:TFrmAddGood;
begin
     Frm1:=TFrmAddGood.Create(Self);
     Frm1.ShowModal;
     FreeAndNil(Frm1);
end;

end.

